var express = require('express');
var app = express();
var repository =require("./data/repository");
var serverData = new repository.repository();
var bodyParser = require('body-parser');



// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

require('./route')(app, serverData);
var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Adres serwera: http://%s:%s", host, port);
  });

  