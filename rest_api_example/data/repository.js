module.exports.repository = function () {

    var fs = require("fs");
    repositoryData =[];

    fs.readFile( __dirname + "/" + "data.json", 'utf8', function (err, data) {
        console.log(data);
        repositoryData=data.replaceAll("\\","").replace('"{','{').replace('}"',"}");
    });

    this.getData = function(){
        return repositoryData;
    }
    this.setData = function(data){
        repositoryData = data;
    }

    this.save = function(){
        fs.writeFile(__dirname + "/" + "data.json",JSON.stringify(repositoryData));
    }
    String.prototype.replaceAll = function(str1, str2, ignore) 
    {
        return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
    } 
};