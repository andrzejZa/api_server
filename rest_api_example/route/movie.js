module.exports = function(app,data) {
   

    app.get('/movies', function (req, res) {
        console.log("GET MOVIES");
        res.end(data.getData());
     });

     app.post('/addMovie', function (req, res) {
        console.log("ADD MOVIE " +req.params.id);
        //console.log("BODY:" + req.body);
        console.log("BODY:" + req.body.data);
        if (req.body == undefined){
            console.log("data is: " + req.body);
            res.end("Failed");
        }
            movies = JSON.parse(data.getData());
            var newMovie = JSON.parse(req.body.data);
            newMovie.id = movies.movies.length;
            console.log(newMovie);
            movies.movies.push(newMovie);
            data.setData(JSON.stringify(movies));
            data.save();
            res.end(JSON.stringify(JSON.parse(req.body.data)));
       
     });

     app.post('/movies/:id', function (req, res) {
        console.log("POST MOVIES " +req.params.id);
        console.log("BODY:" + req.body.data)
        var movies = JSON.parse(data.getData());

        //edit movie
        var movieData = JSON.parse(req.body.data);
        var moviesArr = movies["movies"];
        for (var i=0;i<moviesArr.length;i++){
            if (moviesArr[i].id == req.params.id){
                moviesArr[i].title = movieData.title;
                moviesArr[i].description = movieData.description;
            }
        }
        data.setData(JSON.stringify(movies));
            data.save();
        res.end(JSON.stringify(movieData));
     
  });

     app.get('/movies/:id', function (req, res) {
        
           var movies = JSON.parse(data.getData());
           var movie = movies["movies"][req.params.id];
           res.end( JSON.stringify(movie));
        
     });
     app.delete('/deleteMovie/:id', function (req, res) {
            var movies = JSON.parse(data.getData());
            var moviesArr = movies.movies;
            var index =-1;
            for (var i=0;i<moviesArr.length;i++){
                if (moviesArr[i].id == req.params.id){
                   index  = i;
                   break;
                }
            }
            if (index >-1)
            moviesArr.splice(index, 1);
            
            data.setData(JSON.stringify(movies));
            data.save();
            res.end( JSON.stringify("OK"));
     })
     

    
};